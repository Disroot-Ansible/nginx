# SPDX-FileCopyrightText: 2023 XWiki CryptPad Team <contact@cryptpad.org> and contributors
#
# SPDX-License-Identifier: AGPL-3.0-or-later

{% extends "core.j2" %}

{% block app_specific %}

    # Include mime.types to be able to support .mjs files (see "types" below)
    include mime.types;

    # CryptPad serves static assets over these two domains.
    # `main_domain` is what users will enter in their address bar.
    # Privileged computation such as key management is handled in this scope
    # UI content is loaded via the `sandbox_domain`.
    # "Content Security Policy" headers prevent content loaded via the sandbox
    # from accessing privileged information.
    # These variables must be different to take advantage of CryptPad's sandboxing techniques.
    # In the event of an XSS vulnerability in CryptPad's front-end code
    # this will limit the amount of information accessible to attackers.
    set $main_domain "{{ item.ssl_name }}";
    set $sandbox_domain "sandbox.{{ item.ssl_name }}";

    # By default CryptPad forbids remote domains from embedding CryptPad documents in iframes.
    # The sandbox domain must always be permitted in order for the platform to function.
    # If you wish to enable remote embedding you may change the value below to "*"
    # as per the commented value.
    set $allowed_origins "https://${sandbox_domain}";
    #set $allowed_origins "*";

    # CryptPad's dynamic content (websocket traffic and encrypted blobs)
    # can be served over separate domains. Using dedicated domains (or subdomains)
    # for these purposes allows you to move them to a separate machine at a later date
    # if you find that a single machine cannot handle all of your users.
    # If you don't use dedicated domains, this can be the same as $main_domain
    # If you do, they can be added as exceptions to any rules which block connections to remote domains.
    # You can find these variables referenced below in the relevant places
    set $api_domain "{{ item.ssl_name }}";
    set $files_domain "{{ item.ssl_name }}";

    add_header Access-Control-Allow-Origin "${allowed_origins}";
    add_header Access-Control-Allow-Credentials true;
    # add_header X-Frame-Options "SAMEORIGIN";

    # Opt out of Google's FLoC Network
    add_header Permissions-Policy interest-cohort=();

    # Enable SharedArrayBuffer in Firefox (for .xlsx export)
    add_header Cross-Origin-Resource-Policy cross-origin;
    add_header Cross-Origin-Embedder-Policy require-corp;

    # any static assets loaded with "ver=" in their URL will be cached for a year
    if ($args ~ ver=) {
        set $cacheControl max-age=31536000;
    }
    # This rule overrides the above caching directive and makes things somewhat less efficient.
    # We had inverted them as an optimization, but Safari 16 introduced a bug that interpreted
    # some important headers incorrectly when loading these files from cache.
    # This is why we can't have nice things :(
    if ($uri ~ ^(?:/|.*/|.*\.html)$) {
        set $cacheControl no-cache;
    }

    # Will not set any header if it is emptystring
    add_header Cache-Control $cacheControl;

    # CSS can be dynamically set inline, loaded from the same domain, or from $main_domain
    set $styleSrc   "'unsafe-inline' 'self' https://${main_domain}";

    # connect-src restricts URLs which can be loaded using script interfaces
    # if you have configured your instance to use a dedicated $files_domain or $api_domain
    # you will need to add them below as: https://${files_domain} and https://${api_domain}
    set $connectSrc "'self' https://${main_domain} blob: wss://${api_domain} https://${sandbox_domain}";

    # fonts can be loaded from data-URLs or the main domain
    set $fontSrc    "'self' data: https://${main_domain}";

    # images can be loaded from anywhere, though we'd like to deprecate this as it allows the use of images for tracking
    set $imgSrc     "'self' data: blob: https://${main_domain}";

    # frame-src specifies valid sources for nested browsing contexts.
    # this prevents loading any iframes from anywhere other than the sandbox domain
    set $frameSrc   "'self' https://${sandbox_domain} blob:";

    # specifies valid sources for loading media using video or audio
    set $mediaSrc   "blob:";

    # defines valid sources for webworkers and nested browser contexts
    # deprecated in favour of worker-src and frame-src
    set $childSrc   "https://${main_domain}";

    # specifies valid sources for Worker, SharedWorker, or ServiceWorker scripts.
    # supercedes child-src but is unfortunately not yet universally supported.
    set $workerSrc  "'self'";

    # script-src specifies valid sources for javascript, including inline handlers
    set $scriptSrc  "'self' resource: https://${main_domain}";

    # frame-ancestors specifies which origins can embed your CryptPad instance
    # this must include 'self' and your main domain (over HTTPS) in order for CryptPad to work
    # if you have enabled remote embedding via the admin panel then this must be more permissive.
    # note: cryptpad.fr permits web pages served via https: and vector: (element desktop app)
    set $frameAncestors "'self' {{ item.frameancestors | default('https://${main_domain}:') }}";
    # set $frameAncestors "'self' https: vector:";

    set $unsafe 0;
    # the following assets are loaded via the sandbox domain
    # they unfortunately still require exceptions to the sandboxing to work correctly.
    if ($uri ~ ^/(?:sheet|doc|presentation)/inner.html) { set $unsafe 1; }
    if ($uri ~ ^/common/onlyoffice/.*/.*\.html) { set $unsafe 1; }

    # everything except the sandbox domain is a privileged scope, as they might be used to handle keys
    if ($host != $sandbox_domain) { set $unsafe 0; }
    # this iframe is an exception. Office file formats are converted outside of the sandboxed scope
    # because of bugs in Chromium-based browsers that incorrectly ignore headers that are supposed to enable
    # the use of some modern APIs that we require when javascript is run in a cross-origin context.
    # We've applied other sandboxing techniques to mitigate the risk of running WebAssembly in this privileged scope
    if ($uri ~ ^/unsafeiframe/inner\.html) { set $unsafe 1; }

    # privileged contexts allow a few more rights than unprivileged contexts, though limits are still applied
    if ($unsafe) {
        set $scriptSrc "'self' 'unsafe-eval' 'unsafe-inline' resource: https://${main_domain}";
    }

    # Finally, set all the rules you composed above.
    add_header Content-Security-Policy "default-src 'none'; child-src $childSrc; worker-src $workerSrc; media-src $mediaSrc; style-src $styleSrc; script-src $scriptSrc; connect-src $connectSrc; font-src $fontSrc; img-src $imgSrc; frame-src $frameSrc; frame-ancestors $frameAncestors";

    # Add support for .mjs files used by pdfjs
    types {
        application/javascript mjs;
    }

{% endblock %}

{% block root %}

    root {{ nginx_www_dir }}{{ item.root }};
    index index.html;
    error_page 404 /customize.dist/404.html;

{% endblock %}
{% block location%}
    # The nodejs process can handle all traffic whether accessed over websocket or as static assets
    # We prefer to serve static content from nginx directly and to leave the API server to handle
    # the dynamic content that only it can manage. This is primarily an optimization
    location ^~ /cryptpad_websocket {
        # XXX
        # static assets like blobs and blocks are served by clustered workers in the API server
        # Websocket traffic still needs to be handled by the main process, which means it needs
        # to be hosted on a different port. By default 3003 will be used, though this is configurable
        # via config.websocketPort
        proxy_pass http://{{ item.proxy_pass }}:3003;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        # WebSocket support (nginx 1.4)
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection upgrade;
    }

    location ^~ /customize.dist/ {
        # This is needed in order to prevent infinite recursion between /customize/ and the root
    }
    # try to load customizeable content via /customize/ and fall back to the default content
    # located at /customize.dist/
    # This is what allows you to override behaviour.
    location ^~ /customize/ {
        rewrite ^/customize/(.*)$ $1 break;
        try_files /customize/$uri /customize.dist/$uri;
    }

    # /api/config is loaded once per page load and is used to retrieve
    # the caching variable which is applied to every other resource
    # which is loaded during that session.
    location ^~ /api/ {
        proxy_pass http://{{ item.proxy_pass }}:3000;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        # These settings prevent both NGINX and the API server
        # from setting the same headers and creating duplicates
        proxy_hide_header Cross-Origin-Resource-Policy;
        add_header Cross-Origin-Resource-Policy cross-origin;
        proxy_hide_header Cross-Origin-Embedder-Policy;
        add_header Cross-Origin-Embedder-Policy require-corp;
    }
    location = /extensions.js/ {
        proxy_pass http://{{ item.proxy_pass }}:3000;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        # These settings prevent both NGINX and the API server
        # from setting the same headers and creating duplicates
        proxy_hide_header Cross-Origin-Resource-Policy;
        add_header Cross-Origin-Resource-Policy cross-origin;
        proxy_hide_header Cross-Origin-Embedder-Policy;
        add_header Cross-Origin-Embedder-Policy require-corp;
    }

    # Requests for blobs and blocks are now proxied to the API server
    # This simplifies NGINX path configuration in the event they are being hosted in a non-standard location
    # or with odd unexpected permissions. Serving blobs in this manner also means that it will be possible to
    # enforce access control for them, though this is not yet implemented.
    # Access control (via TOTP 2FA) has been added to blocks, so they can be handled with the same directives.
    location ~ ^/(?:blob|block)/ {
        if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' "${allowed_origins}";
            add_header 'Access-Control-Allow-Credentials' true;
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
            add_header 'Access-Control-Allow-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range';
            add_header 'Access-Control-Max-Age' 1728000;
            add_header 'Content-Type' 'application/octet-stream; charset=utf-8';
            add_header 'Content-Length' 0;
            return 204;
        }
        # Since we are proxying to the API server these headers can get duplicated
        # so we hide them
        proxy_hide_header 'X-Content-Type-Options';
        proxy_hide_header 'Access-Control-Allow-Origin';
        proxy_hide_header 'Permissions-Policy';
        proxy_hide_header 'X-XSS-Protection';
        proxy_hide_header 'Cross-Origin-Resource-Policy';
        proxy_hide_header 'Cross-Origin-Embedder-Policy';
        proxy_pass http://{{ item.proxy_pass }}:3000;
    }

    # The nodejs server has some built-in forwarding rules to prevent
    # URLs like /pad from resulting in a 404. This simply adds a trailing slash
    # to a variety of applications.
    location ~ ^/(?:register|login|recovery|settings|user|pad|drive|poll|slide|code|whiteboard|file|media|profile|contacts|todo|filepicker|debug|kanban|sheet|support|admin|notifications|teams|calendar|presentation|doc|form|report|convert|checkup|diagram)$ {
        return 301 https://$host$uri/;
    }

    # Finally, serve anything the above exceptions don't govern.
    try_files /customize/www/$uri /customize/www/$uri/index.html /www/$uri /www/$uri/index.html /customize/$uri;
{% endblock %}
