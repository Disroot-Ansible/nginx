---

- name: "[SSL] - Create local cert Directory"
  file:
    path: "{{ nginx_ssl_dir }}"
    state: directory
    mode: 0755

- name: "[SSL] - Generate DH file"
  command: openssl dhparam -out {{ nginx_dh_path }} {{ nginx_dh_length }}
  args:
    creates: "{{ nginx_dh_path }}"
  when: nginx_gen_dh == 'true'
  notify:
    - reload nginx

- name: "[SSL] - Deploy DH file from vars"
  copy:
    content: "{{ nginx_dh }}"
    dest: "{{ nginx_dh_path }}"
  when: nginx_dh is defined
  notify:
    - reload nginx

- name: "[SSL] - Create Cert subfolder for {{ item.name }}"
  file:
    path: "{{ nginx_ssl_dir }}/{{ item.ssl_name }}"
    state: directory
    mode: 0755
  with_items: "{{ nginx_vhosts }}"
  when: item.copy_ssl is defined
  notify: reload nginx

- name: "[OWNCERT] - Copy PrivKey from Ansible host for {{ item.name }} from commercial CA"
  copy:
    src: "{{ ssl_src_path }}/{{ item.ssl_name }}/privkey.pem"
    dest: "{{ nginx_ssl_dir}}/{{ item.ssl_name }}/privkey.pem"
    mode: 0700
  with_items: "{{ nginx_vhosts }}"
  when: item.copy_ssl is defined
  notify: reload nginx

- name: "[OWNCERT] - Copy Cert from Ansible host for {{ item.name }} from commercial CA"
  copy:
    src: "{{ ssl_src_path }}/{{ item.ssl_name }}/fullchain.pem"
    dest: "{{ nginx_ssl_dir}}/{{ item.ssl_name }}/fullchain.pem"
    mode: 0644
  with_items: "{{ nginx_vhosts }}"
  when: item.copy_ssl is defined
  notify: reload nginx

- name: "[SELFSIGNED] - Install dependencies"
  apt:
    name: "{{ item }}"
    state: present
  with_items: "{{ nginx_selfsigned_deps }}"
  when: item.selfsigned is defined and item.selfsigned == 'true'

- name: "[SELFSIGNED] - Add python2 cryptography module"
  apt:
    name: python-cryptography
    state: present
  with_items: "{{ nginx_vhosts }}"
  when: item.selfsigned is defined and item.selfsigned == 'true' and ansible_python.executable == '/usr/bin/python'

- name: "[SELFSIGNED] - Add python3 cryptography module"
  apt:
    name: python3-cryptography
    state: present
  with_items: "{{ nginx_vhosts }}"
  when: item.selfsigned is defined and item.selfsigned == 'true' and ansible_python.executable == '/usr/bin/python3'

- name: "[SELFSIGNED] - Create Key folder for {{ item.name }}"
  file:
    path: "{{ nginx_ssl_dir }}/{{ item.ssl_name }}"
    state: directory
    mode: 0755
  with_items: "{{ nginx_vhosts }}"
  when: item.selfsigned is defined and item.selfsigned == 'true' 
  notify: reload nginx

- name: '[SELFSIGNED] - Create a self-signed key for {{ item.name }}'
  openssl_privatekey:
    path:  '{{ ssl_src_path }}/{{ item.ssl_name }}/privkey.pem'
    size: 2048
    type: RSA
  with_items: "{{ nginx_vhosts }}"
  when: item.selfsigned is defined and item.state == 'enable' and item.selfsigned == 'true' 
  notify: reload nginx

- name: '[SELFSIGNED] - Generate OpenSSL Certificate Signing Request (CSR) for {{ item.name }}'
  openssl_csr:
    path: '{{ ssl_src_path }}/{{ item.ssl_name }}/selfsigned.crs'
    privatekey_path:  '{{ ssl_src_path }}/{{ item.ssl_name }}/privkey.pem'
  with_items: "{{ nginx_vhosts }}"
  when: item.selfsigned is defined and item.state == 'enable' and item.selfsigned == 'true'  and (item.wildcard is not defined or item.wildcard != 'true')
  notify: reload nginx

- name: '[SELFSIGNED] - Generate OpenSSL Certificate Signing Request (CSR) for wildcard'
  openssl_csr:
    path:  '{{ ssl_src_path }}/{{ item.ssl_name }}/selfsigned.crs'
    privatekey_path:  '{{ ssl_src_path }}/{{ item.ssl_name }}/privkey.pem'
    common_name: "*.{{ item.domain_name }}"
    subject_alt_name: "DNS:*.{{ item.domain_name }}"
  with_items: "{{ nginx_vhosts }}"
  when: item.selfsigned is defined and item.state == 'enable' and item.selfsigned == 'true' and item.wildcard is defined and item.wildcard == 'true'
  notify: reload nginx

- name: '[SELFSIGNED] - Create a self-signed certificate for {{ item.name }}'
  openssl_certificate:
    path:  '{{ ssl_src_path }}/{{ item.ssl_name }}/fullchain.pem'
    privatekey_path:  '{{ ssl_src_path }}/{{ item.ssl_name }}/privkey.pem'
    csr_path:  '{{ ssl_src_path }}/{{ item.ssl_name }}/selfsigned.crs'
    provider: selfsigned
  with_items: "{{ nginx_vhosts }}"
  when: item.selfsigned is defined and item.state == 'enable' and item.selfsigned == 'true'
  notify: reload nginx

